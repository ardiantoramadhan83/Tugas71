const initialState = {
  userData: {
    nama: '',
    email: '',
    username: '',
    access_token: '',
  },
  isLoggedIn: false,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        isLoggedIn: true,
        userData: action.data,
      };
    case 'LOGOUT':
      return {
        ...state,
        userData: {
          nama: '',
          email: '',
          username: '',
          access_token: '',
        },
        isLoggedIn: false,
      };
    default:
      return state;
  }
};
export default authReducer;
